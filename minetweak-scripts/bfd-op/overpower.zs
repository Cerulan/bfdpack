// ProjectE Tome of Knowledge
recipes.addShapeless(<ProjectE:item.pe_tome>, [<minecraft:book>, <ore:dustRedstone>, <ore:dustRedstone>, <ore:gemDiamond>, <ore:gemDiamond>]);

// Random Things - Fertilized Dirt
recipes.addShapeless(<RandomThings:fertilizedDirt>, [<ore:dirt>, <ore:dustRedstone>]);

// Dirt to Cobble
recipes.addShapeless(<minecraft:cobblestone>, [<ore:dirt>]);

// Dirt Bed
recipes.addShaped(<minecraft:bed>, [[<ore:dirt>, <ore:dirt>, <ore:dirt>], [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>]]);

// Increase Torches
recipes.removeShaped(<minecraft:torch>, [[<ore:coal>], [<ore:stickWood>]]);
recipes.addShaped(<minecraft:torch> * 32, [[<ore:coal>], [<ore:stickWood>]]);

// Glowstone Dust Quickly
recipes.addShapeless(<minecraft:glowstone_dust>, [<ore:coal>, <ore:coal>, <ore:dustRedstone>, <ore:dustRedstone>]);

// LAVA
recipes.addShapeless(<minecraft:lava_bucket>, [<minecraft:bucket>, <ore:dustRedstone>]);

// Division Sigil
recipes.removeShapeless(<minecraft:iron_ingot>, [<ExtraUtilities:divisionSigil>]);
recipes.addShapeless(<ExtraUtilities:divisionSigil>.withTag({damage: 1024}), [<ExtraUtilities:divisionSigil>]);

// Stable Ingots
val stableUnstable = <ExtraUtilities:unstableingot>.withTag({stable: 1 as byte});
recipes.addShaped(stableUnstable, [[<minecraft:iron_ingot>], [<ExtraUtilities:divisionSigil>], [<minecraft:emerald>]]);