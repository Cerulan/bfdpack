import mods.nei.NEI;

NEI.hide(<Mekanism:MachineBlock2:11>);

// Remove Uranium Block Recipe
recipes.remove(<IC2:blockMetal:3>);

// Uranium Oredict Merge
<ore:blockYellorium>.addAll(<ore:blockUranium>);
<ore:blockUranium>.mirror(<ore:blockYellorium>);

// Thaumcraft Gold Coin to Ingot
recipes.addShapeless(<minecraft:gold_ingot>,
	[<Thaumcraft:ItemResource:18>, <Thaumcraft:ItemResource:18>, <Thaumcraft:ItemResource:18>, 
	<Thaumcraft:ItemResource:18>, <Thaumcraft:ItemResource:18>, <Thaumcraft:ItemResource:18>,
	<Thaumcraft:ItemResource:18>, <Thaumcraft:ItemResource:18>, <Thaumcraft:ItemResource:18>]);

// Slag
recipes.addShapeless(<ThermalExpansion:material:515>*4, [<ore:slimeball>, <Thaumcraft:ItemZombieBrain>, <ore:compressedDirt1x>]);

// Extra Utilities Colored to Normal
recipes.addShapeless(<minecraft:stonebrick>, [<ExtraUtilities:colorStoneBrick:*>]);
recipes.addShapeless(<minecraft:planks>, [<ExtraUtilities:colorWoodPlanks:*>]);
recipes.addShapeless(<minecraft:glowstone>, [<ExtraUtilities:color_lightgem:*>]);
recipes.addShapeless(<minecraft:stone>, [<ExtraUtilities:color_stone:*>]);
recipes.addShapeless(<minecraft:quartz_block>, [<ExtraUtilities:color_quartzBlock:*>]);
recipes.addShapeless(<minecraft:soul_sand>, [<ExtraUtilities:color_hellsand:*>]);
recipes.addShapeless(<minecraft:redstone_lamp>, [<ExtraUtilities:color_redstoneLight:*>]);
recipes.addShapeless(<minecraft:brick_block>, [<ExtraUtilities:color_brick:*>]);
recipes.addShapeless(<minecraft:cobblestone>, [<ExtraUtilities:color_stonebrick:*>]);
recipes.addShapeless(<minecraft:lapis_block>, [<ExtraUtilities:color_blockLapis:*>]);
recipes.addShapeless(<minecraft:obsidian>, [<ExtraUtilities:color_obsidian:*>]);
recipes.addShapeless(<minecraft:redstone_block>, [<ExtraUtilities:color_blockRedstone:*>]);
recipes.addShapeless(<minecraft:coal_block>, [<ExtraUtilities:color_blockCoal:*>]);