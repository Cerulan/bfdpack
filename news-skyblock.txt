Skyblock Manufactory Changelog

=== 1.0.4.2 Beta ===
 ! Not much changed...

=== 1.0.4 Beta ===
 ! Fixed crashes (hopefully)
 ! Because Modular Armor is gone, will replace with something else soon
 - Thaumcraft and related mods
 - Modular armor, which needs Thaumcraft for some reason

=== 1.0.3 Beta ===
 ! Fixed World Generation
 ! Fixed Veinminer
 ~ Added config file for YUNoMakeGoodMap
 ~ Added cfgs for Veinminer

=== 1.0.2 Beta ===
 ! Fixed Critical Crash
 ~ Updated LibSandstone

=== 1.0.1 Beta ===
 ! Fixed Skyblock Map
 + Iguana Tinker Tweaks
 + Magical Crops
 + MagicalCrops / MFR Compat
 + Ranchable Fluid Cows
 + Reliquary
 + SolarFlux
 + YUNoMakeGoodMap
 - DragonAPI

=== 1.0.0 Beta ===
First Release
