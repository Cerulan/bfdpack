  ____    _____   ____                             
 | __ )  |  ___| |  _ \                            
 |  _ \  | |_    | | | |                           
 | |_) | |  _|   | |_| |                           
 |______ |_|     |____/               _            
 |  \/  | ___   __| |_ __   __ _  ___| | __        
 | |\/| |/ _ \ / _` | '_ \ / _` |/ __| |/ /        
 | |  | | (_) | (_| | |_) | (_| | (__|   <         
 |_|  |_|\___/ \__,_| .__/ \__,_|\___|_|\_\        
  _____ _____ _____ _____ _____ _____ _____ _____  
 |_____|_____|_____|_____|_____|___________|_____| 
  / ___| |__   __ _ _ __   __ _  ___| | ___   __ _ 
 | |   | '_ \ / _` | '_ \ / _` |/ _ \ |/ _ \ / _` |
 | |___| | | | (_| | | | | (_| |  __/ | (_) | (_| |
  \____|_| |_|\__,_|_| |_|\__, |\___|_|\___/ \__, |
                          |___/              |___/ 
			

=== 1.2.1 ===
- Updated NEI Integration
 > Fixes crash on recipe view
			
=== 1.2.0 ===
- Updated Buildcraft to version 7.0.13
- Updated Buildcraft-Compat to version 7.0.8
- Updated Logistics Pipes to compatible line
- Updated Buildcraft Additions
- Updated Forestry
- Updated Computronics
- Updated OpenPeripheral Suite
- Updated AsieLib
- Updated OpenMods
- Updated OpenBlocks
  > Fixes Fluid Void Pipe disconnecting clients
			
=== 1.1.1 ===
- Updated Tinker's Construct
  > Fixes crash with smeltery
			
=== 1.1.0 ===
- Removed Magical Crops
  > Fixes crash when attempting to use the "Uses" function from NEI


----------------------------		

	
=== 1.0.0 ===
First LITE Release